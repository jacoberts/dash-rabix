webpackHotUpdatedash_rabix("main",{

/***/ "./src/lib/components/DashRabix.react.js":
/*!***********************************************!*\
  !*** ./src/lib/components/DashRabix.react.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DashRabix; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var cwl_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! cwl-svg */ "./node_modules/cwl-svg/compiled/index.js");
/* harmony import */ var cwl_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(cwl_svg__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var cwlts_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! cwlts/models */ "./node_modules/cwlts/models/index.js");
/* harmony import */ var cwlts_models__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(cwlts_models__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./style.scss */ "./src/lib/components/style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_4__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






/**
 * DashRabix displays a CWL workflow using Rabix SVG.
 * It takes a CWL in JSON format.
 */

var DashRabix =
/*#__PURE__*/
function (_Component) {
  _inherits(DashRabix, _Component);

  function DashRabix(props) {
    var _this;

    _classCallCheck(this, DashRabix);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DashRabix).call(this, props));
    var wf = cwlts_models__WEBPACK_IMPORTED_MODULE_3__["WorkflowFactory"].from(JSON.parse(props.cwlWorkflow));
    _this.state = {
      'svgRef': react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef(),
      'rootWorkflowFactory': wf,
      'workflowRenderer': null,
      'selectedNode': null,
      'renderRaw': false,
      'selectedSubworkflow': []
    };
    return _this;
  }

  _createClass(DashRabix, [{
    key: "getHeader",
    value: function getHeader() {
      var _this2 = this;

      var visitSelectedNode = function visitSelectedNode() {
        console.log("Visiting " + _this2.state.selectedNode);
        var newSelection = (_this2.state.selectedSubworkflow || []).concat([_this2.state.selectedNode]);

        _this2.setState({
          'selectedSubworkflow': newSelection
        });

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = _this2.state.rootWorkflowFactory.steps[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var step = _step.value;

            if (step.id === _this2.state.selectedNode) {
              alert("TODO: Navigate to " + _this2.state.selectedNode);
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      };

      var selectedNodeOptions = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Selected " + this.state.selectedNode, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: visitSelectedNode
      }, "Expand"));
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", null, this.state.rootWorkflowFactory.label), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, this.state.selectedNode ? selectedNodeOptions : "Select a node..."));
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var styleOverrides = this.props.style || {};
      var style = {
        width: styleOverrides.width || '75%',
        height: styleOverrides.height || '80%',
        position: styleOverrides.position || 'absolute'
      };
      var rawWorkflow = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("pre", null, JSON.stringify(JSON.parse(this.props.cwlWorkflow), null, '\t'));
      var renderedWorkflow = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
        className: "cwl-workflow",
        ref: this.state.svgRef,
        style: style
      });

      var toggleRawWorkflow = function toggleRawWorkflow() {
        _this3.setState({
          'renderRaw': !_this3.state.renderRaw
        });
      };

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: this.props.id
      }, this.props.showHeader ? this.getHeader() : null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, !this.state.renderRaw && this.props.enableAdvancedOptions ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: toggleRawWorkflow
      }, "Show CWLsssst") : null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, this.state.renderRaw ? rawWorkflow : renderedWorkflow));
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this4 = this;

      var selectionPlugin = new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["SelectionPlugin"]();
      selectionPlugin.registerOnSelectionChange(function (v) {
        if (!v) {
          return;
        }

        _this4.setState({
          'selectedNode': v.getAttribute('data-id')
        });
      });
      this.state.svgRef.current.addEventListener('wheel', function (e, _) {
        // Prevent the event from letting the whole page move.
        e.preventDefault();
      }, {
        // Do the capture before it gets to the ZoomPlugin, which
        // sometimes stops propagation.
        'capture': true
      });
      var workflowRenderer = new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["Workflow"]({
        model: this.state.rootWorkflowFactory,
        svgRoot: this.state.svgRef.current,
        plugins: [new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["SVGArrangePlugin"](), new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["SVGEdgeHoverPlugin"](), new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["SVGNodeMovePlugin"]({
          movementSpeed: 10
        }), new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["SVGPortDragPlugin"](), selectionPlugin, new cwl_svg__WEBPACK_IMPORTED_MODULE_2__["ZoomPlugin"]()]
      });
      this.setState({
        'workflowRenderer': workflowRenderer
      });
    }
  }]);

  return DashRabix;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


DashRabix.defaultProps = {};
DashRabix.propTypes = {
  /**
   * The ID used to identify this component in Dash callbacks.
   */
  id: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,

  /**
   * JSON string with CWL workflow to render.
   */
  cwlWorkflow: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,

  /**
   * Object with style fields.
   */
  style: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,

  /**
   * Show advanced interation fields.
   */
  enableAdvancedOptions: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,

  /**
   * Show the title and header.
   */
  showHeader: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,

  /**
   * Dash-assigned callback that should be called to report property changes
   * to Dash, to make them available for callbacks.
   */
  setProps: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9kYXNoX3JhYml4Ly4vc3JjL2xpYi9jb21wb25lbnRzL0Rhc2hSYWJpeC5yZWFjdC5qcyJdLCJuYW1lcyI6WyJEYXNoUmFiaXgiLCJwcm9wcyIsIndmIiwiV29ya2Zsb3dGYWN0b3J5IiwiZnJvbSIsIkpTT04iLCJwYXJzZSIsImN3bFdvcmtmbG93Iiwic3RhdGUiLCJSZWFjdCIsImNyZWF0ZVJlZiIsInZpc2l0U2VsZWN0ZWROb2RlIiwiY29uc29sZSIsImxvZyIsInNlbGVjdGVkTm9kZSIsIm5ld1NlbGVjdGlvbiIsInNlbGVjdGVkU3Vid29ya2Zsb3ciLCJjb25jYXQiLCJzZXRTdGF0ZSIsInJvb3RXb3JrZmxvd0ZhY3RvcnkiLCJzdGVwcyIsInN0ZXAiLCJpZCIsImFsZXJ0Iiwic2VsZWN0ZWROb2RlT3B0aW9ucyIsImxhYmVsIiwic3R5bGVPdmVycmlkZXMiLCJzdHlsZSIsIndpZHRoIiwiaGVpZ2h0IiwicG9zaXRpb24iLCJyYXdXb3JrZmxvdyIsInN0cmluZ2lmeSIsInJlbmRlcmVkV29ya2Zsb3ciLCJzdmdSZWYiLCJ0b2dnbGVSYXdXb3JrZmxvdyIsInJlbmRlclJhdyIsInNob3dIZWFkZXIiLCJnZXRIZWFkZXIiLCJlbmFibGVBZHZhbmNlZE9wdGlvbnMiLCJzZWxlY3Rpb25QbHVnaW4iLCJTZWxlY3Rpb25QbHVnaW4iLCJyZWdpc3Rlck9uU2VsZWN0aW9uQ2hhbmdlIiwidiIsImdldEF0dHJpYnV0ZSIsImN1cnJlbnQiLCJhZGRFdmVudExpc3RlbmVyIiwiZSIsIl8iLCJwcmV2ZW50RGVmYXVsdCIsIndvcmtmbG93UmVuZGVyZXIiLCJXb3JrZmxvdyIsIm1vZGVsIiwic3ZnUm9vdCIsInBsdWdpbnMiLCJTVkdBcnJhbmdlUGx1Z2luIiwiU1ZHRWRnZUhvdmVyUGx1Z2luIiwiU1ZHTm9kZU1vdmVQbHVnaW4iLCJtb3ZlbWVudFNwZWVkIiwiU1ZHUG9ydERyYWdQbHVnaW4iLCJab29tUGx1Z2luIiwiQ29tcG9uZW50IiwiZGVmYXVsdFByb3BzIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsIm9iamVjdCIsImJvb2wiLCJzZXRQcm9wcyIsImZ1bmMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7Ozs7O0lBSXFCQSxTOzs7OztBQUNqQixxQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBOztBQUNmLG1GQUFNQSxLQUFOO0FBQ0EsUUFBTUMsRUFBRSxHQUFHQyw0REFBZSxDQUFDQyxJQUFoQixDQUFxQkMsSUFBSSxDQUFDQyxLQUFMLENBQVdMLEtBQUssQ0FBQ00sV0FBakIsQ0FBckIsQ0FBWDtBQUNBLFVBQUtDLEtBQUwsR0FBYTtBQUNULGdCQUFVQyw0Q0FBSyxDQUFDQyxTQUFOLEVBREQ7QUFFVCw2QkFBdUJSLEVBRmQ7QUFHVCwwQkFBb0IsSUFIWDtBQUlULHNCQUFnQixJQUpQO0FBS1QsbUJBQWEsS0FMSjtBQU1ULDZCQUF1QjtBQU5kLEtBQWI7QUFIZTtBQVdsQjs7OztnQ0FFVztBQUFBOztBQUNSLFVBQU1TLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsR0FBTTtBQUM1QkMsZUFBTyxDQUFDQyxHQUFSLENBQVksY0FBYyxNQUFJLENBQUNMLEtBQUwsQ0FBV00sWUFBckM7QUFDQSxZQUFNQyxZQUFZLEdBQUcsQ0FBQyxNQUFJLENBQUNQLEtBQUwsQ0FBV1EsbUJBQVgsSUFBa0MsRUFBbkMsRUFBdUNDLE1BQXZDLENBQThDLENBQUMsTUFBSSxDQUFDVCxLQUFMLENBQVdNLFlBQVosQ0FBOUMsQ0FBckI7O0FBQ0EsY0FBSSxDQUFDSSxRQUFMLENBQWM7QUFBQyxpQ0FBdUJIO0FBQXhCLFNBQWQ7O0FBSDRCO0FBQUE7QUFBQTs7QUFBQTtBQUk1QiwrQkFBbUIsTUFBSSxDQUFDUCxLQUFMLENBQVdXLG1CQUFYLENBQStCQyxLQUFsRCw4SEFBeUQ7QUFBQSxnQkFBOUNDLElBQThDOztBQUNyRCxnQkFBSUEsSUFBSSxDQUFDQyxFQUFMLEtBQVksTUFBSSxDQUFDZCxLQUFMLENBQVdNLFlBQTNCLEVBQXlDO0FBQ3JDUyxtQkFBSyxDQUFDLHVCQUF1QixNQUFJLENBQUNmLEtBQUwsQ0FBV00sWUFBbkMsQ0FBTDtBQUNIO0FBQ0o7QUFSMkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVMvQixPQVREOztBQVVBLFVBQU1VLG1CQUFtQixHQUNyQix3RUFBTSxjQUFjLEtBQUtoQixLQUFMLENBQVdNLFlBQS9CLEVBQ0k7QUFBUSxlQUFPLEVBQUVIO0FBQWpCLGtCQURKLENBREo7QUFNQSxhQUFRLHdFQUNKLHVFQUNLLEtBQUtILEtBQUwsQ0FBV1csbUJBQVgsQ0FBK0JNLEtBRHBDLENBREksRUFJSix1RUFDSyxLQUFLakIsS0FBTCxDQUFXTSxZQUFYLEdBQTBCVSxtQkFBMUIsR0FBZ0Qsa0JBRHJELENBSkksQ0FBUjtBQVFIOzs7NkJBRVE7QUFBQTs7QUFDTCxVQUFNRSxjQUFjLEdBQUcsS0FBS3pCLEtBQUwsQ0FBVzBCLEtBQVgsSUFBb0IsRUFBM0M7QUFDQSxVQUFNQSxLQUFLLEdBQUc7QUFDVkMsYUFBSyxFQUFFRixjQUFjLENBQUNFLEtBQWYsSUFBd0IsS0FEckI7QUFFVkMsY0FBTSxFQUFFSCxjQUFjLENBQUNHLE1BQWYsSUFBeUIsS0FGdkI7QUFHVkMsZ0JBQVEsRUFBRUosY0FBYyxDQUFDSSxRQUFmLElBQTJCO0FBSDNCLE9BQWQ7QUFLQSxVQUFNQyxXQUFXLEdBQUcsd0VBQU0xQixJQUFJLENBQUMyQixTQUFMLENBQWUzQixJQUFJLENBQUNDLEtBQUwsQ0FBVyxLQUFLTCxLQUFMLENBQVdNLFdBQXRCLENBQWYsRUFBbUQsSUFBbkQsRUFBeUQsSUFBekQsQ0FBTixDQUFwQjtBQUNBLFVBQU0wQixnQkFBZ0IsR0FDbEI7QUFBSyxpQkFBUyxFQUFDLGNBQWY7QUFBOEIsV0FBRyxFQUFFLEtBQUt6QixLQUFMLENBQVcwQixNQUE5QztBQUFzRCxhQUFLLEVBQUVQO0FBQTdELFFBREo7O0FBRUEsVUFBTVEsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixHQUFNO0FBQzVCLGNBQUksQ0FBQ2pCLFFBQUwsQ0FBYztBQUFDLHVCQUFhLENBQUMsTUFBSSxDQUFDVixLQUFMLENBQVc0QjtBQUExQixTQUFkO0FBQ0gsT0FGRDs7QUFJQSxhQUNJO0FBQUssVUFBRSxFQUFFLEtBQUtuQyxLQUFMLENBQVdxQjtBQUFwQixTQUNLLEtBQUtyQixLQUFMLENBQVdvQyxVQUFYLEdBQXdCLEtBQUtDLFNBQUwsRUFBeEIsR0FBMkMsSUFEaEQsRUFFSSx3RUFDSyxDQUFDLEtBQUs5QixLQUFMLENBQVc0QixTQUFaLElBQXlCLEtBQUtuQyxLQUFMLENBQVdzQyxxQkFBcEMsR0FBNEQ7QUFBUSxlQUFPLEVBQUVKO0FBQWpCLHlCQUE1RCxHQUF5SCxJQUQ5SCxDQUZKLEVBS0ksc0VBTEosRUFNSSx3RUFBTSxLQUFLM0IsS0FBTCxDQUFXNEIsU0FBWCxHQUF1QkwsV0FBdkIsR0FBcUNFLGdCQUEzQyxDQU5KLENBREo7QUFXSDs7O3dDQUVtQjtBQUFBOztBQUNoQixVQUFNTyxlQUFlLEdBQUcsSUFBSUMsdURBQUosRUFBeEI7QUFDQUQscUJBQWUsQ0FBQ0UseUJBQWhCLENBQTBDLFVBQUFDLENBQUMsRUFBSTtBQUMzQyxZQUFJLENBQUNBLENBQUwsRUFBUTtBQUNKO0FBQ0g7O0FBQ0QsY0FBSSxDQUFDekIsUUFBTCxDQUFjO0FBQUMsMEJBQWdCeUIsQ0FBQyxDQUFDQyxZQUFGLENBQWUsU0FBZjtBQUFqQixTQUFkO0FBQ0gsT0FMRDtBQU9BLFdBQUtwQyxLQUFMLENBQVcwQixNQUFYLENBQWtCVyxPQUFsQixDQUEwQkMsZ0JBQTFCLENBQTJDLE9BQTNDLEVBQW9ELFVBQUNDLENBQUQsRUFBR0MsQ0FBSCxFQUFTO0FBQ3pEO0FBQ0FELFNBQUMsQ0FBQ0UsY0FBRjtBQUNILE9BSEQsRUFHRztBQUNDO0FBQ0E7QUFDQSxtQkFBVztBQUhaLE9BSEg7QUFRQSxVQUFNQyxnQkFBZ0IsR0FBRyxJQUFJQyxnREFBSixDQUFhO0FBQ2xDQyxhQUFLLEVBQUUsS0FBSzVDLEtBQUwsQ0FBV1csbUJBRGdCO0FBRWxDa0MsZUFBTyxFQUFFLEtBQUs3QyxLQUFMLENBQVcwQixNQUFYLENBQWtCVyxPQUZPO0FBR2xDUyxlQUFPLEVBQUUsQ0FDTCxJQUFJQyx3REFBSixFQURLLEVBRUwsSUFBSUMsMERBQUosRUFGSyxFQUdMLElBQUlDLHlEQUFKLENBQXNCO0FBQ2xCQyx1QkFBYSxFQUFFO0FBREcsU0FBdEIsQ0FISyxFQU1MLElBQUlDLHlEQUFKLEVBTkssRUFPTG5CLGVBUEssRUFRTCxJQUFJb0Isa0RBQUosRUFSSztBQUh5QixPQUFiLENBQXpCO0FBY0EsV0FBSzFDLFFBQUwsQ0FBYztBQUFDLDRCQUFvQmdDO0FBQXJCLE9BQWQ7QUFDSDs7OztFQXBHa0NXLCtDOzs7QUF1R3ZDN0QsU0FBUyxDQUFDOEQsWUFBVixHQUF5QixFQUF6QjtBQUVBOUQsU0FBUyxDQUFDK0QsU0FBVixHQUFzQjtBQUNsQjs7O0FBR0F6QyxJQUFFLEVBQUUwQyxpREFBUyxDQUFDQyxNQUpJOztBQU1sQjs7O0FBR0ExRCxhQUFXLEVBQUV5RCxpREFBUyxDQUFDQyxNQUFWLENBQWlCQyxVQVRaOztBQVdsQjs7O0FBR0F2QyxPQUFLLEVBQUVxQyxpREFBUyxDQUFDRyxNQWRDOztBQWdCbEI7OztBQUdBNUIsdUJBQXFCLEVBQUV5QixpREFBUyxDQUFDSSxJQW5CZjs7QUFxQmxCOzs7QUFHQS9CLFlBQVUsRUFBRTJCLGlEQUFTLENBQUNJLElBeEJKOztBQTBCbEI7Ozs7QUFJQUMsVUFBUSxFQUFFTCxpREFBUyxDQUFDTTtBQTlCRixDQUF0QixDIiwiZmlsZSI6ImIyOWU4MDUtbWFpbi13cHMtaG1yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7Q29tcG9uZW50fSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQge1dvcmtmbG93LCBTVkdBcnJhbmdlUGx1Z2luLCBTVkdOb2RlTW92ZVBsdWdpbiwgU1ZHUG9ydERyYWdQbHVnaW4sIFNlbGVjdGlvblBsdWdpbiwgU1ZHRWRnZUhvdmVyUGx1Z2luLCBab29tUGx1Z2lufSBmcm9tIFwiY3dsLXN2Z1wiO1xuaW1wb3J0IHtXb3JrZmxvd0ZhY3Rvcnl9IGZyb20gXCJjd2x0cy9tb2RlbHNcIjtcbmltcG9ydCAnLi9zdHlsZS5zY3NzJ1xuXG5cbi8qKlxuICogRGFzaFJhYml4IGRpc3BsYXlzIGEgQ1dMIHdvcmtmbG93IHVzaW5nIFJhYml4IFNWRy5cbiAqIEl0IHRha2VzIGEgQ1dMIGluIEpTT04gZm9ybWF0LlxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEYXNoUmFiaXggZXh0ZW5kcyBDb21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgY29uc3Qgd2YgPSBXb3JrZmxvd0ZhY3RvcnkuZnJvbShKU09OLnBhcnNlKHByb3BzLmN3bFdvcmtmbG93KSk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICAnc3ZnUmVmJzogUmVhY3QuY3JlYXRlUmVmKCksXG4gICAgICAgICAgICAncm9vdFdvcmtmbG93RmFjdG9yeSc6IHdmLFxuICAgICAgICAgICAgJ3dvcmtmbG93UmVuZGVyZXInOiBudWxsLFxuICAgICAgICAgICAgJ3NlbGVjdGVkTm9kZSc6IG51bGwsXG4gICAgICAgICAgICAncmVuZGVyUmF3JzogZmFsc2UsXG4gICAgICAgICAgICAnc2VsZWN0ZWRTdWJ3b3JrZmxvdyc6IFtdXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZ2V0SGVhZGVyKCkge1xuICAgICAgICBjb25zdCB2aXNpdFNlbGVjdGVkTm9kZSA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiVmlzaXRpbmcgXCIgKyB0aGlzLnN0YXRlLnNlbGVjdGVkTm9kZSk7XG4gICAgICAgICAgICBjb25zdCBuZXdTZWxlY3Rpb24gPSAodGhpcy5zdGF0ZS5zZWxlY3RlZFN1YndvcmtmbG93IHx8IFtdKS5jb25jYXQoW3RoaXMuc3RhdGUuc2VsZWN0ZWROb2RlXSk7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsnc2VsZWN0ZWRTdWJ3b3JrZmxvdyc6IG5ld1NlbGVjdGlvbn0pXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHN0ZXAgb2YgdGhpcy5zdGF0ZS5yb290V29ya2Zsb3dGYWN0b3J5LnN0ZXBzKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN0ZXAuaWQgPT09IHRoaXMuc3RhdGUuc2VsZWN0ZWROb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiVE9ETzogTmF2aWdhdGUgdG8gXCIgKyB0aGlzLnN0YXRlLnNlbGVjdGVkTm9kZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBjb25zdCBzZWxlY3RlZE5vZGVPcHRpb25zID0gKFxuICAgICAgICAgICAgPGRpdj57XCJTZWxlY3RlZCBcIiArIHRoaXMuc3RhdGUuc2VsZWN0ZWROb2RlfVxuICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17dmlzaXRTZWxlY3RlZE5vZGV9PkV4cGFuZDwvYnV0dG9uPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuICg8ZGl2PlxuICAgICAgICAgICAgPGgxPlxuICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLnJvb3RXb3JrZmxvd0ZhY3RvcnkubGFiZWx9XG4gICAgICAgICAgICA8L2gxPlxuICAgICAgICAgICAgPGgzPlxuICAgICAgICAgICAgICAgIHt0aGlzLnN0YXRlLnNlbGVjdGVkTm9kZSA/IHNlbGVjdGVkTm9kZU9wdGlvbnMgOiBcIlNlbGVjdCBhIG5vZGUuLi5cIn1cbiAgICAgICAgICAgIDwvaDM+XG4gICAgICAgIDwvZGl2Pik7XG4gICAgfVxuXG4gICAgcmVuZGVyKCkge1xuICAgICAgICBjb25zdCBzdHlsZU92ZXJyaWRlcyA9IHRoaXMucHJvcHMuc3R5bGUgfHwge307XG4gICAgICAgIGNvbnN0IHN0eWxlID0ge1xuICAgICAgICAgICAgd2lkdGg6IHN0eWxlT3ZlcnJpZGVzLndpZHRoIHx8ICc3NSUnLFxuICAgICAgICAgICAgaGVpZ2h0OiBzdHlsZU92ZXJyaWRlcy5oZWlnaHQgfHwgJzgwJScsXG4gICAgICAgICAgICBwb3NpdGlvbjogc3R5bGVPdmVycmlkZXMucG9zaXRpb24gfHwgJ2Fic29sdXRlJyxcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3QgcmF3V29ya2Zsb3cgPSA8cHJlPntKU09OLnN0cmluZ2lmeShKU09OLnBhcnNlKHRoaXMucHJvcHMuY3dsV29ya2Zsb3cpLCBudWxsLCAnXFx0Jyl9PC9wcmU+O1xuICAgICAgICBjb25zdCByZW5kZXJlZFdvcmtmbG93ID0gKFxuICAgICAgICAgICAgPHN2ZyBjbGFzc05hbWU9XCJjd2wtd29ya2Zsb3dcIiByZWY9e3RoaXMuc3RhdGUuc3ZnUmVmfSBzdHlsZT17c3R5bGV9Pjwvc3ZnPik7XG4gICAgICAgIGNvbnN0IHRvZ2dsZVJhd1dvcmtmbG93ID0gKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7J3JlbmRlclJhdyc6ICF0aGlzLnN0YXRlLnJlbmRlclJhd30pO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2IGlkPXt0aGlzLnByb3BzLmlkfT5cbiAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5zaG93SGVhZGVyID8gdGhpcy5nZXRIZWFkZXIoKSA6IG51bGx9XG4gICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgeyF0aGlzLnN0YXRlLnJlbmRlclJhdyAmJiB0aGlzLnByb3BzLmVuYWJsZUFkdmFuY2VkT3B0aW9ucyA/IDxidXR0b24gb25DbGljaz17dG9nZ2xlUmF3V29ya2Zsb3d9PlNob3cgQ1dMc3Nzc3Q8L2J1dHRvbj4gOiBudWxsfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxocj48L2hyPlxuICAgICAgICAgICAgICAgIDxkaXY+e3RoaXMuc3RhdGUucmVuZGVyUmF3ID8gcmF3V29ya2Zsb3cgOiByZW5kZXJlZFdvcmtmbG93fVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIGNvbnN0IHNlbGVjdGlvblBsdWdpbiA9IG5ldyBTZWxlY3Rpb25QbHVnaW4oKTtcbiAgICAgICAgc2VsZWN0aW9uUGx1Z2luLnJlZ2lzdGVyT25TZWxlY3Rpb25DaGFuZ2UodiA9PiB7XG4gICAgICAgICAgICBpZiAoIXYpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsnc2VsZWN0ZWROb2RlJzogdi5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKX0pO1xuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLnN0YXRlLnN2Z1JlZi5jdXJyZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3doZWVsJywgKGUsXykgPT4ge1xuICAgICAgICAgICAgLy8gUHJldmVudCB0aGUgZXZlbnQgZnJvbSBsZXR0aW5nIHRoZSB3aG9sZSBwYWdlIG1vdmUuXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH0sIHtcbiAgICAgICAgICAgIC8vIERvIHRoZSBjYXB0dXJlIGJlZm9yZSBpdCBnZXRzIHRvIHRoZSBab29tUGx1Z2luLCB3aGljaFxuICAgICAgICAgICAgLy8gc29tZXRpbWVzIHN0b3BzIHByb3BhZ2F0aW9uLlxuICAgICAgICAgICAgJ2NhcHR1cmUnOiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgICBjb25zdCB3b3JrZmxvd1JlbmRlcmVyID0gbmV3IFdvcmtmbG93KHtcbiAgICAgICAgICAgIG1vZGVsOiB0aGlzLnN0YXRlLnJvb3RXb3JrZmxvd0ZhY3RvcnksXG4gICAgICAgICAgICBzdmdSb290OiB0aGlzLnN0YXRlLnN2Z1JlZi5jdXJyZW50LFxuICAgICAgICAgICAgcGx1Z2luczogW1xuICAgICAgICAgICAgICAgIG5ldyBTVkdBcnJhbmdlUGx1Z2luKCksXG4gICAgICAgICAgICAgICAgbmV3IFNWR0VkZ2VIb3ZlclBsdWdpbigpLFxuICAgICAgICAgICAgICAgIG5ldyBTVkdOb2RlTW92ZVBsdWdpbih7XG4gICAgICAgICAgICAgICAgICAgIG1vdmVtZW50U3BlZWQ6IDEwXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgbmV3IFNWR1BvcnREcmFnUGx1Z2luKCksXG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uUGx1Z2luLFxuICAgICAgICAgICAgICAgIG5ldyBab29tUGx1Z2luKCksXG4gICAgICAgICAgICBdXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsnd29ya2Zsb3dSZW5kZXJlcic6IHdvcmtmbG93UmVuZGVyZXJ9KTtcbiAgICB9XG59XG5cbkRhc2hSYWJpeC5kZWZhdWx0UHJvcHMgPSB7fTtcblxuRGFzaFJhYml4LnByb3BUeXBlcyA9IHtcbiAgICAvKipcbiAgICAgKiBUaGUgSUQgdXNlZCB0byBpZGVudGlmeSB0aGlzIGNvbXBvbmVudCBpbiBEYXNoIGNhbGxiYWNrcy5cbiAgICAgKi9cbiAgICBpZDogUHJvcFR5cGVzLnN0cmluZyxcblxuICAgIC8qKlxuICAgICAqIEpTT04gc3RyaW5nIHdpdGggQ1dMIHdvcmtmbG93IHRvIHJlbmRlci5cbiAgICAgKi9cbiAgICBjd2xXb3JrZmxvdzogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuXG4gICAgLyoqXG4gICAgICogT2JqZWN0IHdpdGggc3R5bGUgZmllbGRzLlxuICAgICAqL1xuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuXG4gICAgLyoqXG4gICAgICogU2hvdyBhZHZhbmNlZCBpbnRlcmF0aW9uIGZpZWxkcy5cbiAgICAgKi9cbiAgICBlbmFibGVBZHZhbmNlZE9wdGlvbnM6IFByb3BUeXBlcy5ib29sLFxuXG4gICAgLyoqXG4gICAgICogU2hvdyB0aGUgdGl0bGUgYW5kIGhlYWRlci5cbiAgICAgKi9cbiAgICBzaG93SGVhZGVyOiBQcm9wVHlwZXMuYm9vbCxcblxuICAgIC8qKlxuICAgICAqIERhc2gtYXNzaWduZWQgY2FsbGJhY2sgdGhhdCBzaG91bGQgYmUgY2FsbGVkIHRvIHJlcG9ydCBwcm9wZXJ0eSBjaGFuZ2VzXG4gICAgICogdG8gRGFzaCwgdG8gbWFrZSB0aGVtIGF2YWlsYWJsZSBmb3IgY2FsbGJhY2tzLlxuICAgICAqL1xuICAgIHNldFByb3BzOiBQcm9wVHlwZXMuZnVuY1xufTtcbiJdLCJzb3VyY2VSb290IjoiIn0=