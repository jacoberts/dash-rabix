# AUTO GENERATED FILE - DO NOT EDIT

dashRabix <- function(id=NULL, cwlWorkflow=NULL, style=NULL, showHeader=NULL) {
    
    props <- list(id=id, cwlWorkflow=cwlWorkflow, style=style, showHeader=showHeader)
    if (length(props) > 0) {
        props <- props[!vapply(props, is.null, logical(1))]
    }
    component <- list(
        props = props,
        type = 'DashRabix',
        namespace = 'dash_rabix',
        propNames = c('id', 'cwlWorkflow', 'style', 'showHeader'),
        package = 'dashRabix'
        )

    structure(component, class = c('dash_component', 'list'))
}
