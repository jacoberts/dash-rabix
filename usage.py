import json

import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import yaml

import dash_rabix

app = dash.Dash(__name__)

with open('tests/example_workflow.cwl', 'r') as f:
    workflow_contents_yaml = f.read()
    workflow_contents_obj = yaml.safe_load(workflow_contents_yaml)
    workflow_contents_json = json.dumps(workflow_contents_obj)

app.layout = html.Div([
    dash_rabix.DashRabix(
        id='input',
        cwlWorkflow=workflow_contents_json,
        showHeader=True
    )
])


if __name__ == '__main__':
    app.run_server(debug=True)
