import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {Workflow, SVGArrangePlugin, SVGNodeMovePlugin, SVGPortDragPlugin, SelectionPlugin, SVGEdgeHoverPlugin, ZoomPlugin} from "cwl-svg";
import {WorkflowFactory} from "cwlts/models";
import './style.scss'
import YAML from 'yaml'


/**
 *  Holds a WorkflowModel and lazily loads subworkflows w/ memoization.
 */
class WorkflowNode {
    constructor(workflowModel) {
        this.workflowModel = workflowModel;
        this.cachedSubworkflows = {};
    }

    getModel() {
        return this.workflowModel;
    }

    isStep(id) {
        for (const step of this.workflowModel.steps) {
            if (step.id === id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Async recursively calls getStep.
     */
    async followPath(path) {
        if (!path.length) {
            return this;
        }
        const subworkflow = await this.getStep(path[0]);
        return subworkflow.followPath(path.slice(1));
    }

    /**
     * Returns a promise of a WorkflowNode for the corresponding step.
     */
    async getStep(id) {
        if (id in this.cachedSubworkflows) {
            console.log("HIT CACHE");
            return this.cachedSubworkflows[id];
        }
        for (const step of this.workflowModel.steps) {
            if (step.id === id) {
                console.log("MISSED CACHE");
                this.cachedSubworkflows[id] = await WorkflowNode.createFromUrl(step.runPath);
                return this.cachedSubworkflows[id];
            }
        }
        return null;
    }

    printSteps() {
        console.log(this.workflowModel);
    }

    static async maybeFixGitlabUrl(url) {
        // Modify URLs of the form:
        // https://gitlab.com/iidsgt/biocwl/raw/a0fc1d452fc09389e3957f320967bd8a3a949a0e/Workflows/DAPHNI-RNA-Pre.cwl
        const regex = /https:\/\/gitlab\.com\/(?<group>[^/]+)\/(?<projectName>[^/]+)\/raw\/(?<commit>[^/]+)\/(?<file>.+)/;
        const match = regex.exec(url);
        if (!match) {
            return url;
        }

        console.log(`Processing a gitlab raw url: ${url}`);
        const group = match.groups.group;
        const projectName = match.groups.projectName;
        const commit = match.groups.commit;
        const file = match.groups.file;

        const groupProjectsReq = await fetch(`https://gitlab.com/api/v4/groups/${group}/projects`);
        const groupProjects = await groupProjectsReq.json()
        
        var projectId = null;
        for (const project of groupProjects) {
            if (project.name === projectName) {
                projectId = project.id;
            }
        }
        if (!projectId) {
            console.log(`Found no projects named ${projectName} under group ${group}`);
            return url;
        }

        return `https://gitlab.com/api/v4/projects/${projectId}/repository/files/${encodeURIComponent(file)}/raw?ref=${commit}`;
    }

    static async createFromUrl(rawUrl) {
        var url = await WorkflowNode.maybeFixGitlabUrl(rawUrl);
        console.log(`Getting resource from ${url}`);

        const resp = await fetch(url);
        const workflowStr = await resp.text();
        return new WorkflowNode(WorkflowFactory.from(YAML.parse(workflowStr)));
    }
};


/**
 * Renders a Workflow within an SVG element.
 */
class DashSvgRenderer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'svgRef': React.createRef()
        };
    }

    render() {
        const styleOverrides = this.props.styleOverrides || {};
        const style = {
            width: styleOverrides.width || '400px',
            height: styleOverrides.height || '400px',
            position: styleOverrides.position || 'relative',
        };
        return <svg className="cwl-workflow" ref={this.state.svgRef} style={style}></svg>;
    }

    componentDidMount() {
        const selectionPlugin = new SelectionPlugin();
        selectionPlugin.registerOnSelectionChange(v => {
            if (v) {
                this.props.nodeSelectedCallback(v.getAttribute('data-id'));
            }
        });

        this.state.svgRef.current.addEventListener('wheel', (e,_) => {
            // Prevent the event from letting the whole page move.
            e.preventDefault();
        }, {
            // Do the capture before it gets to the ZoomPlugin, which
            // sometimes stops propagation.
            'capture': true
        });

        this.workflowRenderer = new Workflow({
            model: this.props.workflowModel,
            svgRoot: this.state.svgRef.current,
            plugins: [
                new SVGArrangePlugin(),
                new SVGEdgeHoverPlugin(),
                new SVGNodeMovePlugin({
                    movementSpeed: 10
                }),
                new SVGPortDragPlugin(),
                selectionPlugin,
                new ZoomPlugin(),
            ]
        });
        this.workflowRenderer.fitToViewport();
    }

    static get propTypes() {
        return {
            workflowModel: PropTypes.any,
            nodeSelectedCallback: PropTypes.func,
            styleOverrides: PropTypes.object,
        };
    }
};


/**
 * DashRabix displays a CWL workflow using Rabix SVG.
 * It takes a CWL in JSON format.
 */
export default class DashRabix extends Component {
    constructor(props) {
        super(props);

        const wm = WorkflowFactory.from(JSON.parse(this.props.cwlWorkflow));
        const workflowNode = new WorkflowNode(wm);
        workflowNode.printSteps();

        const nodeSelectedCallback = (node) => {
            if (this.state.currentWorkflowNode.isStep(node)) {
                this.setState({'selectedStep': node});
            } else {
                this.setState({'selectedStep': null});
            }
        }

        this.state = {
            'svgRef': React.createRef(),
            'nodeSelectedCallback': nodeSelectedCallback,
            'workflowModel': wm,
            'rootWorkflowNode': workflowNode,
            'currentWorkflowNode': workflowNode,
            'selectedStep': null,
            'path': [],
        };
    }

    getHeader() {
        return (<div>
            <h1>
                {this.state.currentWorkflowNode.getModel().label}
            </h1>
        </div>);
    }

    removeNodeFromPath() {
        const newPath = this.state.path;
        newPath.pop();
        this.state.rootWorkflowNode.followPath(newPath).then((node) => {
            this.setState({'path': newPath, 'currentWorkflowNode': node, 'selectedStep': null});
        });
    }

    addSelectedStepToPath() {
        const newPath = this.state.path.concat([this.state.selectedStep]);
        this.state.rootWorkflowNode.followPath(newPath).then((node) => {
            this.setState({'path': newPath, 'currentWorkflowNode': node, 'selectedStep': null});
        });
    }

    getNavBar() {
        const buttons = [];
        if (this.state.path.length) {
            buttons.push(<button onClick={this.removeNodeFromPath.bind(this)}>Up</button>);
        } else {
            buttons.push(<button disabled>Up</button>);
        }

        if (this.state.selectedStep) {
            buttons.push(<button onClick={this.addSelectedStepToPath.bind(this)}>Visit {this.state.selectedStep}</button>);
        } else {
            buttons.push(<button disabled>Select a subworkflow</button>);
        }
        return <div>{buttons}</div>;
    }

    render() {
        return (
            <div id={this.props.id}>
                {this.props.showHeader ? this.getHeader() : null}
                {this.getNavBar()}
                <DashSvgRenderer
                    workflowModel={this.state.currentWorkflowNode.getModel()}
                    nodeSelectedCallback={this.state.nodeSelectedCallback}
                    styleOverrides={this.props.style}
                    key={this.state.path} />
            </div>
        );
    }
}

DashRabix.defaultProps = {};

DashRabix.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    /**
     * JSON string with CWL workflow to render.
     */
    cwlWorkflow: PropTypes.string.isRequired,

    /**
     * Object with style fields.
     */
    style: PropTypes.object,

    /**
     * Show the title and header.
     */
    showHeader: PropTypes.bool,

    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
